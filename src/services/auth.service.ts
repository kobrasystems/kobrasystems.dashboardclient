import axios from 'axios'
import { LoginForm } from '../models/login-form'
import { SignupForm } from '../models/signup-form'

const API_BASE_URL: string = 'https://localhost:5001'

const signup = (signupDetails: SignupForm) => {
  axios
    .post(API_BASE_URL + '/account/signup', signupDetails)
    .then((response) => {
      if (response.data.authToken) {
        localStorage.setItem('authToken', response.data.authToken)
      }

      return response.data
    })
}

const login = (loginDetails: LoginForm) => {
  axios.post(API_BASE_URL + '/account/login', loginDetails).then((response) => {
    if (response.data.authToken) {
      localStorage.setItem('authToken', response.data.authToken)
    }

    return response.data
  })
}

const logout = () => {
  localStorage.removeItem('authToken')
}

const getAuthHeader = () => {
  const authToken = localStorage.getItem('authToken')

  if (authToken) {
    return { Authorization: 'Bearer ' + authToken }
  } else {
    return {}
  }
}

export default {
  signup,
  login,
  logout,
}
