import axios from 'axios'
import Product from '../models/product'

const API_BASE_URL: string = 'https://localhost:5001'

const getProducts = async () => {
  const response = await axios.get<Product[]>(API_BASE_URL + '/product')

  if (response.status !== 200) {
    // TODO: do something with the error here
  }

  return response.data
}

export default {
  getProducts,
}
