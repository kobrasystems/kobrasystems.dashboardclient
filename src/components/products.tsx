import React, { useState, useEffect } from 'react'
import { Button, Container, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWindows } from '@fortawesome/free-brands-svg-icons'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { useHistory } from 'react-router-dom'

import ProductsService from '../services/products.service'
import Product from '../models/product'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: '0',
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  ul: {
    margin: '0',
    padding: '0',
    listStyle: 'none',
  },
  li: {
    display: 'flex',
    height: 58 + 'px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.12)',
    alignItems: 'center',
    textAlign: 'center',
    padding: '16px',
  },
  liInfo: {
    display: 'flex',
    height: 58 + 'px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.12)',
    alignItems: 'center',
    textAlign: 'center',
    padding: '16px',
    justifyContent: 'center',
  },
  liInfoPrimary: {
    display: 'flex',
    height: 58 + 'px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.12)',
    alignItems: 'center',
    textAlign: 'center',
    padding: '16px',
    justifyContent: 'center',
    color: theme.palette.secondary.dark,
  },
  primaryLi: {
    display: 'flex',
    height: 58 + 'px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.12)',
    alignItems: 'center',
    textAlign: 'center',
    padding: '16px',
    backgroundColor: theme.palette.primary.dark,
  },
  primaryLiCentered: {
    display: 'flex',
    height: 58 + 'px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.12)',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    padding: '16px',
    backgroundColor: theme.palette.primary.dark,
  },
  h3: {
    marginLeft: '8px',
  },
}))

function Products() {
  const classes = useStyles()
  const [products, setProducts] = useState<Product[]>([])
  const history = useHistory()

  useEffect(() => {
    ProductsService.getProducts().then((productsFromService) =>
      setProducts(productsFromService),
    )
  }, [products.length])

  function handleSignupClick(productId: number): void {
    history.push(`/signup/${productId}`)
  }

  return (
    <Container maxWidth="lg">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={12} md={4}>
          <Paper className={classes.paper}>
            <ul className={classes.ul}>
              <li className={classes.primaryLi}>
                <FontAwesomeIcon icon={faWindows} />
                <h3 className={classes.h3}>WINDOWS</h3>
              </li>
              <li className={classes.li}>Plan</li>
              <li className={classes.li}>Disk Space</li>
              <li className={classes.li}>Bandwidth/Traffic</li>
              <li className={classes.li}>Hosted Websites</li>
              <li className={classes.li}>MySQL</li>
              <li className={classes.li}>SQL Server</li>
            </ul>
            <ul className={classes.ul}>
              <li className={classes.li}>Control Panel</li>
              <li className={classes.li}>Domain Aliases</li>
              <li className={classes.li}>SubDomains</li>
              <li className={classes.li}>FTP Accounts</li>
            </ul>
            <ul className={classes.ul}>
              <li className={classes.primaryLi}>
                <h3>SOFTWARE VERSIONS</h3>
              </li>
              <li className={classes.li}>Windows Server 2019</li>
              <li className={classes.li}>IIS 10</li>
              <li className={classes.li}>.NET Core 5</li>
              <li className={classes.li}>ASP.NET 2.0/3.5, 4.0/4.5, 4.6</li>
              <li className={classes.li}>PHP 7.0, 7.1, 7.2</li>
              <li className={classes.li}>MariaDB</li>
            </ul>
            <ul className={classes.ul}>
              <li className={classes.primaryLi}>
                <h3>PRICING</h3>
              </li>
              <li className={classes.li}>
                <h2>Annually</h2>
              </li>
              <li className={classes.li}></li>
            </ul>
          </Paper>
        </Grid>

        {products.map((product) => (
          <Grid item xs={12} sm={12} md={4}>
            <Paper className={classes.paper}>
              <ul className={classes.ul}>
                <li className={classes.primaryLiCentered}>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={(e) => handleSignupClick(product.productId)}
                  >
                    Sign Up
                  </Button>
                </li>
                <li className={classes.liInfo}>
                  <h2>{product.name}</h2>
                </li>
                <li className={classes.liInfo}>
                  {product.diskLimitInMb / 1000} GB
                </li>
                <li className={classes.liInfo}>
                  {product.bandwidthInMb === -1
                    ? 'Unlimited'
                    : product.bandwidthInMb / 1000 + ' GB'}
                </li>
                <li className={classes.liInfo}>{product.domainLimit}</li>
                <li className={classes.liInfo}>
                  {product.mySqlDiskLimitInMb} MB
                </li>
                <li className={classes.liInfo}>
                  {product.sqlServerDiskLimitInMb} MB
                </li>
              </ul>
              <ul className={classes.ul}>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfo}>{product.domainAliasLimit}</li>
                <li className={classes.liInfo}>{product.subDomainsLimit}</li>
                <li className={classes.liInfo}>{product.ftpAccountLimit}</li>
              </ul>
              <ul className={classes.ul}>
                <li className={classes.primaryLi}></li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
                <li className={classes.liInfoPrimary}>
                  <FontAwesomeIcon icon={faCheck} />
                </li>
              </ul>
              <ul className={classes.ul}>
                <li className={classes.primaryLi}></li>
                <li className={classes.liInfo}>
                  <h2>${product.annualCost / 100}</h2>
                </li>
                <li className={classes.liInfo}>
                  <Button variant="contained" color="secondary">
                    Sign Up
                  </Button>
                </li>
              </ul>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}

export default Products
