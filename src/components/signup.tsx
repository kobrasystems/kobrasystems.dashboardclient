import React from 'react'
import { Button, Container, Grid, Select, TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import { Formik, FormikProps } from 'formik'
import * as Yup from 'yup'
import { useParams, useHistory } from 'react-router-dom'

import { SignupForm } from '../models/signup-form'
import AuthService from '../services/auth.service'

interface SignupParams {
  id: string
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    '& .MuiTextField-root': {
      //  margin: theme.spacing(1),
      margin: '0 0 16px 0',
      // width: '25ch',
    },
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  formControl: {
    // margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))

function Signup() {
  const classes = useStyles()
  const { id } = useParams<SignupParams>()
  const history = useHistory()

  return (
    <Container maxWidth="lg" style={{ height: 100 + '%' }}>
      <div className={classes.root} style={{ height: 100 + '%' }}>
        <Grid
          container
          spacing={3}
          //   direction="row"
          justify="center"
          // alignItems="center"
          style={{ marginTop: '16px' }}
        >
          <Paper className={classes.paper}>
            <h2>Signup Page</h2>
            <Formik
              initialValues={{
                emailAddress: '',
                password: '',
                firstName: '',
                lastName: '',
                address1: '',
                address2: '',
                city: '',
                state: '',
                countryCode: '',
                phoneNumber: '',
                postalCode: '',
                companyName: '',
                domainName: '',
                productId: parseInt(id, 10),
              }}
              onSubmit={(values: SignupForm, actions) => {
                AuthService.signup(values)
                history.push(`/checkout/${id}`)
                setTimeout(() => {
                  actions.setSubmitting(false)
                }, 500)
              }}
              validationSchema={Yup.object().shape({
                emailAddress: Yup.string()
                  .email('Enter a valid email address')
                  .required('Enter a valid email address'),
                password: Yup.string()
                  .matches(
                    /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()]).{8,64}\S$/,
                    'Enter a strong password',
                  )
                  .required('Please enter a strong password'),
                firstName: Yup.string().required('Please enter a first name'),
                lastName: Yup.string().required('Please enter a last name'),
                address1: Yup.string().required('Please enter an address'),
                address2: Yup.string(),
                city: Yup.string().required('Please enter a city'),
                state: Yup.string().required('Please enter a state'),
                countryCode: Yup.string().required('Please enter a country'),
                phoneNumber: Yup.string().required(
                  'Please enter a phone number',
                ),
                domainName: Yup.string()
                  .matches(
                    /^[a-zA-Z0-9]+(?!.*(([.]|[-])([.]|[-])))[a-zA-Z0-9-]*\.[a-zA-Z0-9.-]*[a-zA-Z0-9]$/,
                    'Please enter a valid domain name',
                  )
                  .required('Please enter a domain name'),
                postalCode: Yup.string().required('Please enter a postal code'),
                companyName: Yup.string(),
              })}
            >
              {(props: FormikProps<SignupForm>) => {
                const {
                  values,
                  touched,
                  errors,
                  handleBlur,
                  handleChange,
                  isSubmitting,
                  handleSubmit,
                } = props

                return (
                  <form autoComplete="off" onSubmit={handleSubmit}>
                    <div>
                      <TextField
                        style={{ marginRight: '8px' }}
                        id="emailAddress"
                        label="Email Address"
                        variant="outlined"
                        type="email"
                        name="emailAddress"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.emailAddress}
                        required
                        helperText={
                          errors.emailAddress && touched.emailAddress
                            ? errors.emailAddress
                            : 'Enter your email address'
                        }
                        error={
                          errors.emailAddress && touched.emailAddress
                            ? true
                            : false
                        }
                      />

                      <TextField
                        id="password"
                        label="Password"
                        type="password"
                        variant="outlined"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        required
                        helperText={
                          errors.password && touched.password
                            ? errors.password
                            : 'Enter a password'
                        }
                        error={
                          errors.password && touched.password ? true : false
                        }
                      />
                    </div>

                    <div>
                      <TextField
                        id="companyName"
                        label="Company Name"
                        variant="outlined"
                        fullWidth
                        type="text"
                        name="companyName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.companyName}
                      />
                    </div>

                    <div>
                      <TextField
                        style={{ marginRight: '8px' }}
                        id="firstName"
                        label="First Name"
                        variant="outlined"
                        type="text"
                        name="firstName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.firstName}
                        required
                        helperText={
                          errors.firstName && touched.firstName
                            ? errors.firstName
                            : 'Enter your first name'
                        }
                        error={
                          errors.firstName && touched.firstName ? true : false
                        }
                      />

                      <TextField
                        id="lastName"
                        label="Last Name"
                        variant="outlined"
                        type="text"
                        name="lastName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.lastName}
                        required
                        helperText={
                          errors.lastName && touched.lastName
                            ? errors.lastName
                            : 'Enter your last name'
                        }
                        error={
                          errors.lastName && touched.lastName ? true : false
                        }
                      />
                    </div>

                    <div>
                      <TextField
                        style={{ marginRight: '8px' }}
                        id="address1"
                        label="Address 1"
                        variant="outlined"
                        type="text"
                        name="address1"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.address1}
                        required
                        helperText={
                          errors.address1 && touched.address1
                            ? errors.address1
                            : 'Enter your address'
                        }
                        error={
                          errors.address1 && touched.address1 ? true : false
                        }
                      />

                      <TextField
                        id="address2"
                        label="Address 2"
                        variant="outlined"
                        type="text"
                        name="address2"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.address2}
                      />
                    </div>

                    <div>
                      <TextField
                        style={{ marginRight: '8px' }}
                        id="phoneNumber"
                        fullWidth
                        label="Phone Number"
                        variant="outlined"
                        type="text"
                        name="phoneNumber"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phoneNumber}
                        required
                        helperText={
                          errors.phoneNumber && touched.phoneNumber
                            ? errors.phoneNumber
                            : 'Enter your phone number'
                        }
                        error={
                          errors.phoneNumber && touched.phoneNumber
                            ? true
                            : false
                        }
                      />
                    </div>

                    <div>
                      <TextField
                        style={{ marginRight: '8px' }}
                        id="city"
                        label="City"
                        variant="outlined"
                        type="text"
                        name="city"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.city}
                        required
                        helperText={
                          errors.city && touched.city
                            ? errors.city
                            : 'Enter your city'
                        }
                        error={errors.city && touched.city ? true : false}
                      />

                      <TextField
                        id="state"
                        label="State"
                        variant="outlined"
                        type="text"
                        name="state"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.state}
                        required
                        helperText={
                          errors.state && touched.state
                            ? errors.state
                            : 'Enter your state'
                        }
                        error={errors.state && touched.state ? true : false}
                      />
                    </div>

                    <div>
                      <TextField
                        id="postalCode"
                        label="Postal Code"
                        variant="outlined"
                        fullWidth
                        type="text"
                        name="postalCode"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.postalCode}
                        required
                        helperText={
                          errors.postalCode && touched.postalCode
                            ? errors.postalCode
                            : 'Enter your postal code'
                        }
                        error={
                          errors.postalCode && touched.postalCode ? true : false
                        }
                      />
                    </div>

                    <div>
                      <FormControl
                        variant="outlined"
                        className={classes.formControl}
                        fullWidth
                      >
                        <InputLabel>Country</InputLabel>
                        <Select
                          labelId="demo-simple-select-outlined-label"
                          id="countryCode"
                          value={values.countryCode}
                          fullWidth
                          onChange={handleChange}
                          name="countryCode"
                          onBlur={handleBlur}
                          label="Age"
                          required
                          variant="outlined"
                          error={
                            errors.countryCode && touched.countryCode
                              ? true
                              : false
                          }
                        >
                          <MenuItem value="US">United States</MenuItem>
                        </Select>
                      </FormControl>
                    </div>

                    <div style={{ marginTop: '16px' }}>
                      <TextField
                        id="domainName"
                        label="Domain Name"
                        variant="outlined"
                        fullWidth
                        type="text"
                        name="domainName"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.domainName}
                        required
                        helperText={
                          errors.domainName && touched.domainName
                            ? errors.domainName
                            : 'Enter your domain name'
                        }
                        error={
                          errors.domainName && touched.domainName ? true : false
                        }
                      />
                    </div>

                    <div style={{ marginTop: '8px' }}>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        disabled={isSubmitting}
                      >
                        Sign Up
                      </Button>
                    </div>
                  </form>
                )
              }}
            </Formik>
          </Paper>
        </Grid>
      </div>
    </Container>
  )
}
export default Signup
