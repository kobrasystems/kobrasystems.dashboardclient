import React from 'react'
import {
  Button,
  Container,
  Grid,
  InputAdornment,
  TextField,
} from '@material-ui/core'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCircle, faLock } from '@fortawesome/free-solid-svg-icons'
import { Formik, Form, FormikProps } from 'formik'
import * as Yup from 'yup'
import { LoginForm } from '../models/login-form'
import AuthService from '../services/auth.service'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    '& .MuiTextField-root': {
      //  margin: theme.spacing(1),
      margin: '0 0 16px 0',
      // width: '25ch',
    },
    // '& > *': {
    //   margin: theme.spacing(1),
    // },
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}))

function Login() {
  const classes = useStyles()
  return (
    <Container maxWidth="sm" style={{ height: 100 + '%' }}>
      <div className={classes.root} style={{ height: 100 + '%' }}>
        <Grid
          container
          spacing={3}
          direction="row"
          justify="center"
          alignItems="center"
          style={{ height: 100 + '%' }}
        >
          <Grid item xs={6} md={8}>
            <Paper className={classes.paper}>
              <h2>Login Page</h2>
              <Formik
                initialValues={{ emailAddress: '', password: '' }}
                onSubmit={(values: LoginForm, actions) => {
                  console.log('login form was submitted')
                  // use auth service to login here
                  AuthService.login(values)
                  setTimeout(() => {
                    actions.setSubmitting(false)
                  }, 500)
                }}
                validationSchema={Yup.object().shape({
                  emailAddress: Yup.string()
                    .email('Enter a valid email address')
                    .required('Enter a valid email address'),
                  password: Yup.string()
                    .matches(
                      /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()]).{8,64}\S$/,
                      'Enter a strong password',
                    )
                    .required('Please enter a strong password'),
                })}
              >
                {(props: FormikProps<LoginForm>) => {
                  const {
                    values,
                    touched,
                    errors,
                    handleBlur,
                    handleChange,
                    isSubmitting,
                    handleSubmit,
                  } = props

                  return (
                    <form autoComplete="off" onSubmit={handleSubmit}>
                      <Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="emailAddress"
                            label="Email Address"
                            variant="outlined"
                            type="email"
                            name="emailAddress"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.emailAddress}
                            required
                            fullWidth
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  <FontAwesomeIcon icon={faUserCircle} />
                                </InputAdornment>
                              ),
                            }}
                            helperText={
                              errors.emailAddress && touched.emailAddress
                                ? errors.emailAddress
                                : 'Enter your email address'
                            }
                            error={
                              errors.emailAddress && touched.emailAddress
                                ? true
                                : false
                            }
                          />
                        </Grid>
                      </Grid>
                      <Grid>
                        <Grid item xs={12}>
                          <TextField
                            id="password"
                            label="Password"
                            type="password"
                            variant="outlined"
                            name="password"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.password}
                            fullWidth
                            required
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position="start">
                                  <FontAwesomeIcon icon={faLock} />
                                </InputAdornment>
                              ),
                            }}
                            helperText={
                              errors.password && touched.password
                                ? errors.password
                                : 'Enter a password'
                            }
                            error={
                              errors.password && touched.password ? true : false
                            }
                          />
                        </Grid>
                      </Grid>

                      <Grid>
                        <Grid item xs={12}>
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            fullWidth
                            disabled={isSubmitting}
                          >
                            Login
                          </Button>
                        </Grid>
                      </Grid>
                    </form>
                  )
                }}
              </Formik>
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Container>
  )
}

export default Login
