import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { createMuiTheme } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import ThemeProvider from './theme-switcher'
import './App.css'

import Home from './components/home'
import Login from './components/login'
import Signup from './components/signup'
import Products from './components/products'
import Checkout from './components/checkout'

function App() {
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
    },
  })

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/signup/:id">
            <Signup />
          </Route>
          <Route path="/products">
            <Products />
          </Route>
          <Route path="/checkout/:id">
            <Checkout />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  )
}

export default App
