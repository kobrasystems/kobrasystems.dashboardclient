export interface SignupForm {
  emailAddress: string
  password: string
  firstName: string
  lastName: string
  address1: string
  address2: string
  city: string
  state: string
  countryCode: string
  phoneNumber: string
  postalCode: string
  companyName: string
  domainName: string
  productId: number
}
