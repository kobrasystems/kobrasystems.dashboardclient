export default interface Product {
  productId: number
  monthlyCost: number
  annualCost: number
  bandwidthInMb: number
  diskLimitInMb: number
  domainAliasLimit: number
  domainLimit: number
  emailAccountLimit: number
  emailDiskLimitInMb: number
  ftpAccountLimit: number
  mySqlDiskLimitInMb: number
  name: string
  description: string
  shownToCustomer: boolean
  sqlServerDiskLimitInMb: number
  subDomainsLimit: number
  stripePriceId: string
  stripeProductId: string
}
