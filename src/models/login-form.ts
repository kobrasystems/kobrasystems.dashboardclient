export interface LoginForm {
  emailAddress: string
  password: string
}
